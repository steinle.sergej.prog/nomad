var builder = WebApplication.CreateBuilder(args);
var app = builder.Build();

app.MapGet("/", () => "Ich bin Hashi Stack");
app.MapGet("/time", () => DateTime.Now.ToString("d"));
app.Run();